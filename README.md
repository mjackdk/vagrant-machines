# Vagrant Machines

Tested on Debian Bookworm and Fedora Workstation 39.

## Debian Setup

The following steps need to be run only once.

```
$ echo 'export VAGRANT_DEFAULT_PROVIDER=libvirt' >> ~/.bashrc
$ sudo apt install vagrant-libvirt libvirt-daemon-system
$ sudo usermod -aG libvirt $USER
$ sudo reboot
```

## Fedora Setup

The following steps need to be run only once.

```
$ echo 'export VAGRANT_DEFAULT_PROVIDER=libvirt' >> ~/.bashrc
$ sudo dnf install vagrant vagrant-libvirt
$ sudo systemctl enable --now virtnetworkd
$ sudo usermod -aG libvirt $USER
$ sudo reboot
```

## Using machines

Navigate to e.g. the 'ubuntu' directory in this repository and run:

```
$ vagrant up
$ vagrant ssh
```

When you're done using the machine for now, exit by typing 'exit' or 'Ctrl-D'.

Halt the machine, if you want to use it later:

```
$ vagrant halt
$ vagrant status
```

Destroy the machine, if you want to remove it completely:

```
$ vagrant destroy
$ vagrant status
```

Check status of all machines:

```
$ vagrant global-status
```
